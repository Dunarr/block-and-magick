// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./Avatar.sol";

contract Avatars {
    mapping (string => address) avatars;

    function createAvatar(int endurance, int force, int intelligence, int vitesse, string memory _type, string memory name) public {
        avatars[name] = address(new Avatar(endurance, force, intelligence, vitesse, _type, name));
    }

    function deleteAvatar(string memory name) public {
        delete avatars[name];
    }

    function getAvatar(string calldata name) external view returns(address){
        return avatars[name];
    }
}