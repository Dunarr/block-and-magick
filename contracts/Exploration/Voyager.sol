pragma solidity >=0.4.22 <0.9.0;

import "../Test/World.sol";

contract Voyager {
    uint64[] public codes;

    function exploreLocation(address wAddr, address avatar, address squad, uint16 location, uint64 code) public payable{
        World world = World(wAddr);
        require(world.estLieuValide(location), "lieu invalide");
        require(!world.estOccupe(location), "Lieu occupé");
        require(!world.avatarExplore(avatar), "Avatar occupé");
        uint cost = world.getCoutLieu(location);
        require(msg.value >= cost, "Montant insuffisant");
        codes.push(world.explorer.value(cost)(location, avatar, code));
        world.appliquerEffetLieu(location, squad, avatar);
        world.quitter(location, avatar);
    }
}
