// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./contracts_interfaces/ActionStrategy.sol";
import "./Avatar.sol";

contract StrategyBeginner is ActionStrategy{
    function chooseNextAction(Action previousAdvAction, address avatar, int damages, address avatarAdv, int damagesAdv) external view returns(Action action){
        Avatar ally = Avatar(avatar);
        Avatar opponent = Avatar(avatarAdv);
        (int end, int forc, int inte, int vit) = opponent.getCaracteristiques();

        if(previousAdvAction != Action.PHYSICAL_ATTACK && previousAdvAction != Action.MAGICAL_ATTACK)
            return forc > inte ? Action.PHYSICAL_PARRY : Action.MAGICAL_PARRY;

        (end, forc, inte, vit) = ally.getCaracteristiques();
        return forc > inte ? Action.PHYSICAL_ATTACK : Action.MAGICAL_ATTACK;
    }
}
