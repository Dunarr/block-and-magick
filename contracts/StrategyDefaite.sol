// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./contracts_interfaces/ActionStrategy.sol";

contract StrategyDefaite is ActionStrategy{
    function chooseNextAction(Action previousAdvAction, address avatar, int damages, address avatarAdv, int damagesAdv) external view returns(Action action){
        return Action.NONE;
    }
}
