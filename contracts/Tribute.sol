// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract Tribute {

  function sendTribute(address payable _to) public payable {
    _to.transfer(msg.value);
  }
}
