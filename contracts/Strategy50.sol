// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./contracts_interfaces/ActionStrategy.sol";
import "./Avatar.sol";

contract Strategy50 is ActionStrategy{
    function chooseNextAction(Action previousAdvAction, address avatar, int damages, address avatarAdv, int damagesAdv) external view returns(Action action){
        Avatar ally = Avatar(avatar);
        Avatar opponent = Avatar(avatarAdv);
        (int end, int forc, int inte, int vit) = ally.getCaracteristiques();

        if(ally.getVie() <= end / 2){
            (end, forc, inte, vit) = opponent.getCaracteristiques();
            if(ally.getVie() % 2 == 1)
                return Action.DODGE;
            return forc >= inte ? Action.PHYSICAL_PARRY : Action.MAGICAL_PARRY;
        }

        return ally.getVie() % 2 == 1 ? (forc >= inte ? Action.PHYSICAL_ATTACK : Action.MAGICAL_ATTACK) : Action.DODGE;
    }
}
