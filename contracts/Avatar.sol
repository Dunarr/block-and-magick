// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./contracts_interfaces/AvatarInterface.sol";

contract Avatar is AvatarInterface {
    int END;
    int FOR;
    int INT;
    int VIT;

    int pv;

    string typ;
    string nom;

    address strat;

    constructor(int endurance, int force, int intelligence, int vitesse, string memory _type, string memory name) public {
        pv = END = endurance;
        FOR = force;
        INT = intelligence;
        VIT = vitesse;
        typ = _type;
        nom = name;
    }

    /**
     * Donne les caractéristiques de l'avatar.
     * @return l'endurance, la force, l'intelligence et la vitesse
     */
    function getCaracteristiques() external view returns(int endurance, int force, int intelligence, int vitesse){
        return (END, FOR, INT, VIT);
    }

    /**
     * Donne le type de l'avatar.
     * @return le nom du type d'avatar
     */
    function getType() external view returns(string memory){
        return typ;
    }

    /**
     * Donne le nom de l'avatar.
     * @return son nom
     */
    function getNom() external view returns(string memory){
        return nom;
    }

    /**
     * Donne la vie actuelle de l'avatar.
     * @return le nombre de points de vie
     */
    function getVie() external view returns(int){
        return pv;
    }

    /**
     * Donne la défense physique de l'avatar
     * @return la valeur de défense physique
     */
    function getDefensePhysique() external view returns(int){
        return END * 15 / 40;
    }

    /**
     * Donne la défense magique de l'avatar
     * @return la valeur de défense magique
     */
    function getDefenseMagique() external view returns(int){
        return END / 4;
    }

    /**
     * Donne l'esquive de l'avatar
     * @return la valeur d'esquive
     */
    function getEsquive() external view returns(int){
        return VIT * 12 / 10;
    }

    /**
     * Donne la puissance physique de l'avatar
     * @return la valeur de puissance physique
     */
    function getPuissancePhysique() external view returns(int){
        return FOR * 2;
    }

    /**
     * Donne la puissance magique de l'avatar
     * @return la valeur de puissance magique
     */
    function getPuissanceMagique() external view returns(int){
        return INT * 314 / 100;
    }

    /**
     * Modifie la stratégie de combat.
     * @param strategie l'adresse de la nouvelle stratégie de combat
     */
    function modifierStrategieCombat(address strategie) external {
        strat = strategie;
    }

    function heal(int hp) external {
        if(hp > 0){
            pv += hp;
            if(pv > END)
                pv = END;
        }
    }

    function hurt(int hp) external {
        if(hp > 0){
            pv -= hp;
            if(pv < 0)
                pv = 0;
        }
    }
}