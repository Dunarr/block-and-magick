// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
import './Avatar.sol';

contract Soin {
  function heal(address avatar, int amount) public {
      Avatar av = Avatar(avatar);
      ( int end, int _hidden1, int _hidden2, int _hidden3 ) = av.getCaracteristiques();
      if(av.getVie() < end)
        av.heal(amount);
  }
}
