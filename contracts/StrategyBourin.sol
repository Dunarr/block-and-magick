// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./contracts_interfaces/ActionStrategy.sol";
import "./Avatar.sol";

contract StrategyBourin is ActionStrategy{
    function chooseNextAction(Action previousAdvAction, address avatar, int damages, address avatarAdv, int damagesAdv) external view returns(Action action){
        Avatar ally = Avatar(avatar);
        (int end, int forc, int inte, int vit) = ally.getCaracteristiques();
        return forc > inte ? Action.PHYSICAL_ATTACK : Action.MAGICAL_ATTACK;
    }
}
